package mysites.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Created by eberroer on 2016/01/10.
 */
public class Sites {
    public ArrayList<FeedObjects> getFeeds(Connection connection) throws Exception{

        ArrayList<FeedObjects> feedData = new ArrayList<FeedObjects>();

        try {
            PreparedStatement ps = connection.prepareStatement("SELECT title,description,url FROM website ORDER BY id DESC");
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                FeedObjects feedObject = new FeedObjects();
                feedObject.setTitle(rs.getString("title"));
                feedObject.setDescription(rs.getString("description"));
                feedObject.setUrl(rs.getString("url"));
                feedData.add(feedObject);
            }
            return feedData;
        }catch(Exception e) {
            throw e;
        }
    }
}
