package mysites.model;

/**
 * Created by eberroer on 2016/01/10.
 */
public class FeedObjects {
    private String title;
    private String description;
    private String url;

    public FeedObjects() {

    }

    public FeedObjects(String title) {
        this.title = title;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
