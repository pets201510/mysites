package mysites.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by eberroer on 2016/01/10.
 */
public class Database {
    public Connection getConnection() throws Exception {
        try {
            String connectionURL = "jdbc:mysql://localhost:3306/messages";
            Connection connection = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connection = DriverManager.getConnection(connectionURL, "root", "root");
            return connection;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }
}
