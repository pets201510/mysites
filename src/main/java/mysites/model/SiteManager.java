package mysites.model;

import java.sql.Connection;
import java.util.ArrayList;

/**
 * Created by eberroer on 2016/01/10.
 */
public class SiteManager {
    public ArrayList<FeedObjects> getFeeds() throws Exception {
        ArrayList<FeedObjects> feeds = null;
        try {
            Database database = new Database();
            Connection connection = database.getConnection();
            Sites project = new Sites();
            feeds = project.getFeeds(connection);
        } catch (Exception e) {
            throw e;
        }
        return feeds;
    }
}
